document.addEventListener("keydown", (event) => {
    if (event.code === "Enter") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("enter-btn").classList.add('blue-btn');

    } else if (event.code === "KeyS") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("s-btn").classList.add('blue-btn');
    } else if (event.code === "KeyE") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("e-btn").classList.add('blue-btn');
    } else if (event.code === "KeyO") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("o-btn").classList.add('blue-btn');
    } else if (event.code === "KeyN") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("n-btn").classList.add('blue-btn');
    } else if (event.code === "KeyL") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("l-btn").classList.add('blue-btn');
    } else if (event.code === "KeyZ") {
        event.preventDefault();
        const activeKey = document.querySelector("#wrapper button.blue-btn");
        if(activeKey){
            activeKey.classList.remove("blue-btn");
        }
        document.getElementById("z-btn").classList.add('blue-btn');
    }
})
